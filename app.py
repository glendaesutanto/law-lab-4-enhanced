import os
from typing import Optional
from fastapi import FastAPI, File, UploadFile, status, HTTPException
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from bson import ObjectId
from bson.binary import Binary
import motor.motor_asyncio

app = FastAPI()
MONGODB_PASSWORD=os.environ["MONGODB_PASSWORD"]
MONGODB_URL=f"mongodb+srv://glendaesutanto:{MONGODB_PASSWORD}@mongodbcluster.2soxp.mongodb.net/test?retryWrites=true&w=majority&tls=true&tlsAllowInvalidCertificates=true"
client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URL)
db = client.lab_4


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")


class Student(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    npm: str
    name: str
    address: str

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "npm": "1806133774",
                "name": "Glenda Emanuella Sutanto",
                "address": "Jl. ABC Cirebon",
            }
        }


class UpdateStudentRequest(BaseModel):
    name: Optional[str]
    address: Optional[str]

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "npm": "1806133774",
                "name": "Glenda Emanuella Sutanto",
                "address": "Jl. ABC Cirebon",
            }
        }


@app.post("/student", response_description="Add new student")
async def create_student(student: Student):
    student = jsonable_encoder(student)
    new_student = await db["students"].insert_one(student)
    created_student = await db["students"].find_one({"_id": new_student.inserted_id})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_student)


@app.get("/students", response_description="Get all students")
async def get_all_students():
    students = await db["students"].find().to_list(1000)
    return students


@app.get("/student/{npm}", response_description="Get student by npm")
async def get_student_by_npm(npm: str):
    if (student := await db["students"].find_one({"npm": npm})) is not None:
        return student

    raise HTTPException(status_code=404, detail=f"Student with npm {npm} not found")


@app.put("/student/{npm}", response_description="Update a student")
async def update_student(npm: str, update_student_request: UpdateStudentRequest):
    new_student_data = {k: v for k, v in update_student_request.dict().items() if v is not None}
    update_result = await db["students"].update_one({"npm": npm}, {"$set": new_student_data})

    if update_result.modified_count == 1:
        if (
            updated_student := await db["students"].find_one({"npm": npm})
        ) is not None:
            return updated_student

    if (existing_student := await db["students"].find_one({"npm": npm})) is not None:
        return existing_student

    raise HTTPException(status_code=404, detail=f"Student with npm {npm} not found")


@app.delete("/student/{npm}", response_description="Delete a student")
async def delete_student(npm: str):
    delete_result = await db["students"].delete_one({"npm": npm})

    if delete_result.deleted_count == 1:
        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content={"message": f"Student with npm {npm} has been deleted successfully."})

    raise HTTPException(status_code=404, detail=f"Student with npm {npm} not found")


@app.post("/uploadfile")
async def upload_file(file: UploadFile = File(...)):
    file_db = client.file_meta

    try:
        with open(file.filename, "rb") as buffer:
            encoded = Binary(buffer.read())
        await file_db["files"].insert_one({"filename": file.filename, "file": encoded})
        return JSONResponse(
            status_code = status.HTTP_200_OK,
            content = {
                "message": f"File {file.filename} has been successfully uploaded."
            }
        )
    except Exception as e:
        print(e)
        return JSONResponse(
            status_code = status.HTTP_400_BAD_REQUEST,
            content = {
                "error": "invalid_request",
                "error_description": "There's an error in your request."
            }
        )
